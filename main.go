package main

import (
	"database/sql"
	"fmt"
	"os"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: ", os.Args[0], "<database>")
		return
	}
	db,err := sql.Open("mysql", os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	var t string
	err = db.QueryRow("SELECT NOW()").Scan(&t)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("OK:",t)
}

